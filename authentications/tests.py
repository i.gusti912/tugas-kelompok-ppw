from django.contrib.auth.models import User
from django.test import TestCase, Client
from django.urls import reverse, resolve
from . import views

class TestUrls(TestCase):
    def test_login_url_resolved(self):
        url = reverse('authentications:login')
        self.assertEquals(resolve(url).func, views.login)
    def test_register_url_resolved(self):
        url = reverse('authentications:register')
        self.assertEquals(resolve(url).func, views.register)
    def test_update_url_resolved(self):
        url = reverse('authentications:update')
        self.assertEquals(resolve(url).func, views.update)
    def test_logout_url_resolved(self):
        url = reverse('authentications:logout')
        self.assertEquals(resolve(url).func, views.logout)

class TestViews(TestCase):
    def test_login_GET(self):
        client = Client()
        response = client.get(reverse('authentications:login'))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response,  'authentications/login.html')
    def test_register_GET(self):
        client = Client()
        response = client.get(reverse('authentications:register'))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response,  'authentications/register.html')

class TestLogin(TestCase):
    def setUp(self):
        self.credentials = {
            'username': 'testuser',
            'password': 'secret'
        }
        self.falseCredentials = {
            'username': 'testuser2',
            'password': 'secret2'
        }
        User.objects.create_user(**self.credentials)
    def test_successful_login(self):
        response1 = self.client.post(reverse('authentications:login'), self.credentials, follow=True)
        self.assertEquals(response1.status_code, 200)
        self.assertJSONEqual(
            str(response1.content, encoding='utf8'),
            {"message": "Success"}
        )

        response2 = self.client.get(reverse('authentications:login'), follow=True)
        self.assertRedirects(response2, '/', status_code=302, target_status_code=200, msg_prefix='', fetch_redirect_response=True)
    def test_unsuccessful_login(self):
        response = self.client.post(reverse('authentications:login'), self.falseCredentials, follow=True)
        self.assertEquals(response.status_code, 401)
        self.assertJSONEqual(
            str(response.content, encoding='utf8'),
            {"message": "Invalid"}
        )

class TestRegister(TestCase):
    def setUp(self):
        self.credentials = {
            'username': 'testuser',
            'password1': '1@2H3f4@5n',
            'password2': '1@2H3f4@5n'
        }
        self.falseCredentials = {
            'username': 'testuser2',
            'password1': '123456ok',
            'password2': '123456ok'
        }
    def test_successful_register(self):
        response1 = self.client.post(reverse('authentications:register'), self.credentials, follow=True)
        self.assertRedirects(response1, '/', status_code=302, target_status_code=200, msg_prefix='', fetch_redirect_response=True)

        response2 = self.client.get(reverse('authentications:register'), follow=True)
        self.assertRedirects(response2, '/', status_code=302, target_status_code=200, msg_prefix='', fetch_redirect_response=True)
    def test_unsuccessful_register(self):
        response = self.client.post(reverse('authentications:register'), self.falseCredentials, follow=True)
        self.assertEquals(response.status_code, 400)
        self.assertTemplateUsed(response, 'authentications/register.html')

class TestUpdate(TestCase):
    def setUp(self):
        self.credentials1 = {
            'username': 'testuser',
            'password': 'secret',
        }
        self.credentials2 = {
            'name': 'Habibullah Akbar',
            'email': 'akbar2habibullah@gmail.com'
        }
        User.objects.create_user(**self.credentials1)
    def test_redirect(self):
        response = self.client.get(reverse('authentications:update'), follow=True)
        self.assertRedirects(response, reverse('authentications:login'), status_code=302, target_status_code=200, msg_prefix='', fetch_redirect_response=True)
    def test_update(self):
        response1 = self.client.post(reverse('authentications:login'), self.credentials1, follow=True)
        response2 = self.client.get(reverse('authentications:update'), follow=True)
        self.assertEquals(response2.status_code, 200)
        self.assertTemplateUsed(response2,  'authentications/update.html')

        response3 = self.client.post(reverse('authentications:update'), self.credentials2, follow=True)
        self.assertEquals(response3.status_code, 200)
        self.assertTemplateUsed(response3,  'authentications/update.html')
        self.assertContains(response3, self.credentials2['name'])
        self.assertContains(response3, self.credentials2['email'])

class TestLogout(TestCase):
    def setUp(self):
        self.credentials = {
            'username': 'testuser',
            'password': 'secret',
        }
        User.objects.create_user(**self.credentials)
    def test_logout(self):
        response1 = self.client.post(reverse('authentications:login'), self.credentials, follow=True)
        self.assertEquals(response1.status_code, 200)
        self.assertJSONEqual(
            str(response1.content, encoding='utf8'),
            {"message": "Success"}
        )

        response2 = self.client.get(reverse('authentications:logout'), follow=True)
        self.assertRedirects(response2, reverse('authentications:login'), status_code=302, target_status_code=200, msg_prefix='', fetch_redirect_response=True)
