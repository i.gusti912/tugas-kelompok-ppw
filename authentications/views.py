from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login as auth_login, logout as auth_logout
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.http import HttpResponse
from django.urls import reverse
from . import forms, models
import json

# Create your views here.

def login(req):
    if req.user.is_authenticated:
        return redirect('/')
    if req.method == 'POST':
        username = req.POST['username']
        password = req.POST['password']
        user = authenticate(req, username=username, password=password)
        if user is not None:
            auth_login(req, user)
            return HttpResponse(json.dumps({"message": "Success"}), content_type="application/json", status=200)
        else:
            return HttpResponse(json.dumps({"message": "Invalid"}), content_type="application/json", status=401)
    else:
        form = AuthenticationForm()
        return render(req, 'authentications/login.html', {'page_title': 'Login Page', 'form': form})

def register(req):
    if req.user.is_authenticated:
        return redirect('/')
    if req.method == 'POST':
        form = UserCreationForm(req.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password1')
            user = authenticate(req, username=username, password=password)
            auth_login(req, user)
            return redirect('/')
        else:
            return render(req, 'authentications/register.html', {'page_title': 'Register Page', 'form': form}, status=400)
    else:
        form = UserCreationForm()
        return render(req, 'authentications/register.html', {'page_title': 'Register Page', 'form': form})

def update(req):
    if req.user.is_authenticated:
        if req.method == 'POST':
            form = forms.AccountUpdateForm(req.POST)
            form.instance.user = req.user
            if form.is_valid():
                form.save()
            return render(req, 'authentications/update.html', {'page_title': 'Update Profile Page', 'form': form})
        else:
            try:
                form = forms.AccountUpdateForm(instance=req.user.account)
            except Exception as e:
                form = forms.AccountUpdateForm()
            return render(req, 'authentications/update.html', {'page_title': 'Update Profile Page', 'form': form})
    else:
        return redirect(reverse('authentications:login'))

def logout(req):
    auth_logout(req)
    return redirect('/authentications/login')
