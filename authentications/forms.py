from django import forms
from . import models

class AccountUpdateForm(forms.ModelForm):
    class Meta:
        model = models.Account
        fields = ['name', 'email']
