from django.shortcuts import render, redirect
from view import models

# Create your views here.

def index(req):
    if req.user.is_authenticated:
        if req.GET.get('search') is not None:
            search = req.GET.get('search')
            data = req.user.todo_set.filter(title__contains=search, finished=True)
        else:
            data = req.user.todo_set.filter(finished=True)
        return render(req, 'archive/index.html', {'page_title': 'Archive Page', 'todo_list': data})
    else:
        return redirect('authentications:login')