$(document).ready(() => {
    $("#login_submit").click(handlesubmit);
});

const handlesubmit = (e) => {
  e.preventDefault();

  $("#errorlogin").html("Please wait...");

  let username = document.getElementById("id_username").value;
  let password = document.getElementById("id_password").value;
  let csrfmiddlewaretoken = document.getElementsByName("csrfmiddlewaretoken")[0]
    .value;

  if (username != "" && password != "") {
    $.ajax({
      url: "/authentications/login/",
      type: "POST",
      data: {
        username,
        password,
        csrfmiddlewaretoken,
      },
      success: (response) => {
        window.location.reload();
      },
      error: (xhr, status, err) => {
        $("#errorlogin").html("The username and Password doesn't match");
      }
    });
  } else {
    $("#errorlogin").html("Plaese enter username and password");
  }
};
