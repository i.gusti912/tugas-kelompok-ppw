from django.shortcuts import render, redirect
from . import forms

# Create your views here.

def index(req):
    if req.user.is_authenticated:
        if req.method == 'POST':
            form = forms.TodoCreateForm(req.POST)
            form.instance.user = req.user
            form.instance.date = req.POST['date']
            if form.is_valid():
                form.save()
            return redirect('view:home')
        else:
            form = forms.TodoCreateForm()
        return render(req, 'add/index.html', {'page_title': 'Add List Page', 'form': form})
    else:
        return redirect('authentications:login')