from django import forms
from view import models

class TodoCreateForm(forms.ModelForm):
    class Meta:
        model = models.Todo
        fields = ['title', 'details']
