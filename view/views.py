from django.shortcuts import render, redirect
from . import models

# Create your views here.

def index(req):
    if req.user.is_authenticated:
        if req.GET.get('search') is not None:
            search = req.GET.get('search')
            data = req.user.todo_set.filter(title__contains=search, finished=False)
        else:
            data = req.user.todo_set.filter(finished=False)
        return render(req, 'view/index.html', {'page_title': 'Home Page', 'todo_list': data})
    else:
        return redirect('authentications:login')

def finish(req):
    if req.user.is_authenticated:
        if req.GET.get('id') is not None:
            id = req.GET.get('id')
            try:
                todo = req.user.todo_set.get(pk=id)
                todo.finished = True
                todo.save()
            except:
                pass
        return redirect('view:home')
    else:
        return redirect('authentications:login')

def delete(req):
    if req.user.is_authenticated:
        if req.GET.get('id') is not None:
            id = req.GET.get('id')
            try:
                todo = req.user.todo_set.get(pk=id)
                todo.delete()
            except:
                pass
        return redirect('view:home')
    else:
        return redirect('authentications:login')
