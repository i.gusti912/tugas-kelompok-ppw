from django.urls import path
from . import views

app_name = 'view'

urlpatterns = [
    path('', views.index, name='home'),
    path('finish/', views.finish, name='finish'),
    path('delete/', views.delete, name='delete'),
]
