# Proyek Akhir PPW

[![pipeline status](https://gitlab.com/i.gusti912/tugas-kelompok-ppw/badges/master/pipeline.svg)](https://gitlab.com/i.gusti912/tugas-kelompok-ppw/-/commits/master) [![coverage report](https://gitlab.com/i.gusti912/tugas-kelompok-ppw/badges/master/coverage.svg)](https://gitlab.com/i.gusti912/tugas-kelompok-ppw/-/commits/master)

Anggota Kelompok:

- Gilang Mahardika Pratama
- Habibullah Akbar
- I Gusti Aswina Mahendra
- Yahya Abdurrohman Alghifari

Link Herokuapp:
https://proyek-akhir-ppw2020.herokuapp.com/

YOK SEMANGAT!

# Tutorial  

- `pip install pipenv`  
- `git clone https://gitlab.com/i.gusti912/tugas-kelompok-ppw.git`
- Aktifin virtualenv pake `pipenv shell`  
- install dependencies django pake `pip install -r requirements.txt`  
- buat branch baru pake `git checkout -b <nama-branch-baru>`  
- App buat masing-masing udah dibikinin, routing dari core juga udah disetting. Tinggal koding buat masing-masing app aja.  
- Selesai koding tinggal push aja sesuai branch masing-masing  

## Tambahan

- kalo mau bikin template html, bikin directory di dalem app aja kek "templates/***nama-app***". Bisa liat contoh di app authentications.  
- Template html bisa extends layout dari 'layouts/base.html', trus isinya ada block style (buat masukin custom css), body, sama script (buat masukin custom javascript).  
- Kalo mau masukin file static (gambar, css, javascript). Bisa taro di folder static, kalo gambar di directory "static/assets", css di "static/style", javascript di "static/script"  
- Jangan lupa kalo mau pake file static masukin `{% load static %}` setelah extends  
- Kalo mau bikin form yang otomatis distyling pake bootstrap bisa pake `{{ <nama-form>|crispy }}`, trus masukin `{% load crispy_forms_tags %}` setelah extends  
- Karena pake autentikasi, jadi tiap view harus ada `if req.user.is_authenticated:` dulu. Kalo false, nanti di redirect ke halaman login.
